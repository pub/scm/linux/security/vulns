#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# Copyright (c) 2024 - Greg Kroah-Hartman <gregkh@linuxfoundation.org>
#
# find_reverts - Search the entries and see if any have commits that have been reverted.
#
# Good to run every once in a while to keep things clean.
#
# Usage:
#	find_reverts [year]
#
# Requires:
#  id_is_reverted

# Colors are good!
if [[ -t 1 ]]; then
	txtred=$(tput setaf 1)		# Red
	txtgrn=$(tput setaf 2)		# Green
	txtylw=$(tput setaf 3)		# Yellow
	txtblu=$(tput setaf 4)		# Blue
	txtcyn=$(tput setaf 6)		# Cyan
	txtrst=$(tput sgr0)		# Text reset
else
	txtred=""
	txtgrn=""
	txtylw=""
	txtblu=""
	txtcyn=""
	txtrst=""
fi

# set where the tool was run from,
# the name of our script,
# and the git version of it
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
SCRIPT=${0##*/}

# FIXME: move to local directory, don't have it just be in Greg's
ID_IS_REVERTED="${DIR}/../tools/verhaal/scripts/id_is_reverted"
if [[ ! -x "${ID_IS_REVERTED}" ]] ; then
	echo "${txtred}Error:${txtrst} the program ${txtblu}${ID_IS_REVERTED}${txtrst} must be present and executable."
	exit 1
fi



help() {
	echo "${SCRIPT}"
	echo "	Note, CVE_USER must be set to your CVE_USER email address"
	exit 1
}

CVE=$1

# don't use unset variables
set -o nounset

cd "${DIR}"/../ || exit 1

update_cve()
{
	local id=$1
	local sha
	local cve

	sha=$(cat "${id}")
	cve=$(echo "${id}" | cut -f 1 -d '.' | cut -f 4 -d '/')
	#echo "id=${id} sha=${sha} cve=${cve}"

	revert=$(${ID_IS_REVERTED} "${sha}")
	if [[ "${revert}" != "" ]]; then
		# test to see if we "know" this is a safe commit
		ok=$(grep ${sha} ${DIR}/not_reverts)
		if [[ "${ok}" == "" ]]; then
			echo "${txtcyn}${cve}${txtrst} with sha ${txtcyn}${sha}${txtrst} has been reverted, check to see if this is still a valid CVE"
		fi
	fi
}

update_year() {
	local year=$1
	local threads

	threads=$(nproc)

	# get a count of ids for this year
	total_count=$(ls cve/published/"${year}"/*.sha1 | wc -l)

	echo "Searching ${txtcyn}${total_count}${txtrst} CVE ids for ${txtgrn}${year}${txtrst} with ${txtcyn}${threads}${txtrst} processes..."
	for id in cve/published/"${year}"/*.sha1 ; do
		while :
		do
			if [[ $(jobs -p | wc -l) -lt ${threads} ]]; then
				#echo "id=${id}"
				update_cve "${id}" &
				break
			else
				sleep 1
			fi
		done
	done
	wait
}

if [[ "${CVE}" == "" ]]; then
	# Nothing specified on the command line, so just update everything by
	# looping through all years
	for y in cve/published/* ; do
		year=$(echo "${y}" | cut -f 3 -d '/')
		update_year "${year}"
	done
else
	# Either the year, or a specific CVE id is specified here.
	#
	# Test for a specific cve id first
	found=$("${DIR}"/cve_search "${CVE}")
	found_result=$?
	if [[ "${found_result}" == "0" ]]; then
		CVE_ROOT="${DIR}/../cve/"
		found=$(find "${CVE_ROOT}" -type f | grep -v testing | grep "${CVE}" | grep "sha1")
		#echo "found='${found}"
		if [[ "${found}" != "" ]]; then
			# strip off the CVE root, as that's what update_cve is expecting:
			update_cve "cve/${found/#$CVE_ROOT}" "1"
			exit 0
		fi
	fi

	# Not a specific id, let's try to do this for a year
	#
	if [[ -d cve/published/${CVE} ]]; then
		update_year "${CVE}"
		exit 0
	fi

	echo "${txtred}ERROR:${txtrst} ${txtcyn}${CVE}${txtrst} is not found or is not a year."
	exit 1
fi

