#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
#
# Copyright (c) 2024 - Greg Kroah-Hartman <gregkh@linuxfoundation.org>
#
# cve_publish_mbox - Send the pending mbox announvements out
#
#
# Usage:
#	cve_publish_mbox
#
# Requires:
#  cve


# don't use unset variables
set -o nounset

# set where the tool was run from,
# the name of our script,
# and the git version of it
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
SCRIPT=${0##*/}
SCRIPT_VERSION=$(cd "${DIR}" && git show -s --abbrev-commit --abbrev=12 --pretty=format:"%h")

help() {
	echo "${SCRIPT}"
	exit 1
}

cd "${DIR}"/../ || exit 1

send_email_files=""

# Get a list of the json files that are modified in some way (not deleted) and
# only submit those to the system, cutting down on round trips
files=$(git status -s -- cve/published | grep -v "^ D" | awk '{print $2}' | grep "\.mbox$" )
for file in ${files}; do
	cve=$(echo "${file}" | cut -f 1 -d '.' | cut -f 4 -d '/')
	root=$(echo "${file}" | cut -f 1 -d '.' )
	sha_file="${root}.sha1"
	sha=$(cat "${sha_file}")
	#echo "file=${file} cve=${cve} sha_file=${sha_file} sha=${sha}"
	#echo "id=${id} sha=${sha} cve=${cve}"
	echo "Sending email for ${cve} for commit ${sha}"
	send_email_files+=" ${file}"
done

git send-email --no-thread --to=linux-cve-announce@vger.kernel.org ${send_email_files}
good=$?
if [[ "${good}" == 0 ]]; then
	echo "Emails successfully sent"
else
	echo "ERROR: Something went wrong sending email ${cve}"
fi
